# mbus2mqtt-home-assistant Docker

## Overview
This Docker image is based on Alpine Linux and includes the `libmbus` library for communicating with M-Bus devices, typically used for reading utility, heating and energy meters. M-Bus is not ModBus btw! It runs a Python script (`mbus2mqtt-home-assistant.py`) to query M-Bus slave devices using libmbus, either via TCP (serial over TCP) or a local serial Modbus device and then sends the data records to mqtt. It also uses Home Assistant compliant MQTT auto discovery.

## WARNING

This project is not more than proof-of-concept quality. It uses the latest Alpine but relies on the very old and unmaintained `libmbus`. Also, the Python code is quite hacky, very opinionated, and primarily made for Home Assistant. It's also only tested with one Kamstrup MULTICAL 602 M-Bus Heating Energy Meter that I have.

## Prerequisites
- Docker or Podman installed on your system.

## Building the Docker Image
1. Clone or download the repository containing the Dockerfile and the `mbus2mqtt-home-assistant` directory.
2. Navigate to the directory with the Dockerfile.
3. Build the image using:
   ```bash
   docker build -t mbus2mqtt-home-assistant .
   ```

## Running the Docker Container
To run the container with the necessary environment variables for MQTT and M-Bus configuration, use the following command:

```bash
docker run -ti -e MQTT_HOST="mqtt.home.local:1883" \
    -e DEVICE_NAME="mbus_mqtt_test1" \
    -e MBUS_REQUEST_CMD="/libmbus/bin/mbus-tcp-request-data 172.17.0.106 5000 0" \
    mbus2mqtt-home-assistant
```

### Environment Variables
* `MQTT_HOST`: The MQTT broker address.
* `DEVICE_NAME`: A name for your M-Bus device (will be the device name in Home Assistant).
* `MBUS_REQUEST_CMD`: The command to request data from the M-Bus device.

## Testing M-Bus
To test the M-Bus connection and find the right command, you can run the image with an interactive shell:

```bash
docker run -ti mbus2mqtt-home-assistant sh
```
Inside the container, you'll find the libmbus tools in /libmbus/bin/. The important tools are:

* `mbus-serial-request-data` (for serial device) or `mbus-tcp-request-data` (for TCP connections) - use this in your `MBUS_REQUEST_CMD`.
* `mbus-serial-scan` (for serial device) or `mbus-tcp-scan` (for TCP connections) - to scan for M-Bus devices.
